package TP1;

import TP1.aasd.*;
import sun.security.krb5.internal.CredentialsUtil;

import java.nio.file.Paths;
import java.io.IOException;

/*
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
*/

public class Main {
  public static void main(String[] args) {
    // Use with a manually made AST
    ASD.Document ast = new ASD.Document(/* Fill here! */);
    System.out.println(ast.toNtriples());

    Entite polyent = new Entite("poly117");
    Sujet poly = new Sujet(polyent);
    Pred typepred = new Pred(new Entite("type"));
    Duet type = new Duet(typepred, new ObjEnt(new Entite("poly")));
    Duet auteur = new Duet(new Pred(new Entite("auteur")), new ObjEnt(new Entite("Ridoux")), new ObjEnt(new Entite("Ferre")));
    Duet titre = new Duet(new Pred(new Entite("titre")), new ObjText("Compilation"));

    Triplet t = new Triplet(poly, type, auteur, titre);

    Triplet t2 = new Triplet(new Sujet(new Entite("Ridoux")), new Duet(new Pred(new Entite("type")), new ObjEnt(new Entite("personne")), new ObjEnt(new Entite("professeur"))));

    System.out.println(new Turtle(t, t2).toNTriples());


    // Use with lexer and parser
    /*
    try {
      // Set input
      CharStream input;
      if(args.length == 0) {
        // From standard input
        input = CharStreams.fromStream(System.in);
      }
      else {
        // From file set in first argument
        input = CharStreams.fromPath(Paths.get(args[0]));
      }

      // Instantiate Lexer
      TurtleLexer lexer = new TurtleLexer(input);
      CommonTokenStream tokens = new CommonTokenStream(lexer);

      // Instantiate Parser
      TurtleParser parser = new TurtleParser(tokens);

      // Parse
      ASD.Document ast = parser.document().out;

      // Print as Ntriples
      System.out.println(ast.toNtriples());
    } catch(IOException e) {
      e.printStackTrace();
    }
    */
  }
}
