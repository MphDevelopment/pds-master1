package TP1.aasd;

import java.util.Arrays;
import java.util.List;

/**
 * Created by 17002843 on 27/09/19.
 */
public class Turtle
{
    private List<Triplet> rdf;

    public Turtle(List<Triplet> rdf)
    {
        this.rdf = rdf;
    }

    public Turtle(Triplet... rdfs)
    {
        this.rdf = Arrays.asList(rdfs);
    }

    public String toNTriples()
    {
        String str = "";
        for(Triplet r : rdf)
        {
            str += r.toNTriples();
        }
        return str;
    }
}
