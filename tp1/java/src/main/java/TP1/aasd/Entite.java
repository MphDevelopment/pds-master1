package TP1.aasd;

/**
 * Created by 17002843 on 27/09/19.
 */
public class Entite
{
    private String str;

    public Entite(String str)
    {
        this.str = str;
    }

    public String toNTriples()
    {
        return "<" + this.str + ">";
    }
}
