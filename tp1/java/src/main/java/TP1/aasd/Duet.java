package TP1.aasd;

import com.sun.istack.internal.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by 17002843 on 27/09/19.
 */
public class Duet
{
    private Pred pred;

    private List<Obj> listObj;

    public Duet(Pred pred, @NotNull List<Obj> listObj)
    {
        this.pred = pred;
        this.listObj = listObj;
    }

    public Duet(Pred pred, @NotNull Obj... objs)
    {
        this.pred = pred;
        this.listObj = Arrays.asList(objs);
    }

    public List<String> toNTriples()
    {
        List<String> str = new ArrayList<>();
        int i = 0;
        for (Obj o : listObj)
        {
            str.add(pred.toNTriples() + " " + o.toNTriples());
        }
        return str;
    }

    //ou public String toNTriples(Sujet sujet)
}