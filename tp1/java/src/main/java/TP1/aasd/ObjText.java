package TP1.aasd;

/**
 * Created by 17002843 on 27/09/19.
 */
public class ObjText implements Obj
{
    private String text;

    public ObjText(String text)
    {
        this.text = text;
    }

    public String toNTriples()
    {
        return "\"" + this.text + "\"";
    }
}
