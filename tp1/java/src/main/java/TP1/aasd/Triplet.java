package TP1.aasd;

import com.sun.istack.internal.NotNull;

import java.util.Arrays;
import java.util.List;

/**
 * Created by 17002843 on 27/09/19.
 */
public class Triplet
{
    private Sujet sujet;
    private List<Duet> listPred;

    public Triplet(Sujet sujet, @NotNull List<Duet> listPred)
    {
        this.sujet = sujet;
        this.listPred = listPred;
    }

    public Triplet(Sujet sujet, @NotNull Duet... preds)
    {
        this.sujet = sujet;
        this.listPred = Arrays.asList(preds);
    }

    public String toNTriples()
    {
        String ret = "";
        String suj = sujet.toNTriples();
        for (Duet d : listPred)
        {
            for (String s : d.toNTriples())
            {
                ret += suj + " " + s + " .\n";
            }
        }
        return ret;
    }
}
