package TP1.aasd;

/**
 * Created by 17002843 on 27/09/19.
 */
public class ObjEnt implements Obj
{
    private Entite ent;

    public ObjEnt(Entite ent)
    {
        this.ent = ent;
    }

    public String toNTriples()
    {
        return this.ent.toNTriples();
    }
}
