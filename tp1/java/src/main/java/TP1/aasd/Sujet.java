package TP1.aasd;

/**
 * Created by 17002843 on 27/09/19.
 */
public class Sujet
{
    private Entite ent;

    public Sujet(Entite ent)
    {
        this.ent = ent;
    }

    public String toNTriples()
    {
        return ent.toNTriples();
    }
}
