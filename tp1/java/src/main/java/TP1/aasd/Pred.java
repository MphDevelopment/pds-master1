package TP1.aasd;

/**
 * Created by 17002843 on 27/09/19.
 */
public class Pred
{
    private Entite ent;

    public Pred(Entite ent)
    {
        this.ent = ent;
    }

    public String toNTriples()
    {
        return ent.toNTriples();
    }
}
